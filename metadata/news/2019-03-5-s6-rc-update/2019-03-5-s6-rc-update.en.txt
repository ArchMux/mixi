Title: s6-rc major version release
Author: Tom Briden <tom@decompile.me.uk>
Content-Type: text/plain
Posted: 2019-03-05
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: sys-apps/s6-rc[<0.5.0.0]

Updating to the latest s6-rc may cause system issues if not done
carefully. As per the release notes:

This is a major version release of s6-rc, but the compiled database
format does not change. It is not necessary to recompile your source
databases. However, it is necessary to stop all services before
upgrading s6 (else the down notifications will not be
received and the s6-rc -d change command will hang.) I recommend
to switch to single-user mode, then upgrade s6, then restart the
supervision tree (or reboot), then restart the s6-rc services,
in order to avoid all problems.
